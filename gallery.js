


//declaracion del modulo gallery, Declaracion del componente gallery
//template url conecta con el gallery.html
//controlador que llama a la funcion gallery component
angular.module("gallery", []).component("gallery", {

  templateUrl: "gallery.html",
  controller: GalleryComponent,
});
//funcion  gallery component que contiene un array de imagenes 
function GalleryComponent() {
  this.array = [
    //arreglo de objetos 4 atributos
    { image: "https://picsum.photos/id/237/400/300" },
    { image: "https://picsum.photos/seed/picsum/400/300" },
    { image: "https://picsum.photos/400/300?grayscale" },
    { image: "https://picsum.photos/id/200/400/300" },
  ];

  //variable 
  this.currentIndex = 0;
//comparacion de que imagen se esta mostrando en el momento
  this.isCurrentIndex = function (index) {
    return this.currentIndex === index;
  };
  //funcion que recorre el arreglo y cambia a la imagen siguiente

  this.nextArray = function () {
        if (this.currentIndex<this.array.length-1){
          this.currentIndex= ++this.currentIndex;
        } else {
         this.currentIndex= 0;
        } 
  };

  //funcion que recorre el arreglo y cambia a la imagen anterior
  this.prevArray = function () {
      if(this.currentIndex>0){
        this.currentIndex= --this.currentIndex
      }else{
       this.currentIndex= this.array.length-1;
      }
  };

}
