angular
  // modulo, commponente ,referencia al html "gallery","contact","home"
  .module("app", ["ngComponentRouter", "gallery", 'contact', 'home'])
  .config(function ($locationProvider) {
    //locationProvider  administra como los links son, true es para como angular maneja la ruta
    $locationProvider.html5Mode(true);
  })
  //$routerRootComponent indica cual es el router/enrutador que ejecuta primero  en este caso app.html
  //value crea o modifica un valor a una variable global en este caso $routerRootComponent
  .value("$routerRootComponent", "app")
  //crea un nuevo componente de nombre app
  .component("app", {
    //manda al app.html
    templateUrl: "app.html",
    $routeConfig: [
      {
        //path modifica la ruta,name jala la ruta de la nueva pagina,component indica el componente que esta en el index.html
        path: "/gallery",
        name: "Gallery",
        component: "gallery",
      },
      {
        path: '/contact',
        name: 'Contact',
        component: 'contact'
      },
      {
        path: '/home',
        name: 'Home',
        component: 'home'
      }
    ],
  });
